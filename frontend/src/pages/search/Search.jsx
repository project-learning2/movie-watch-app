import React, { useState } from "react";

import Navbar from "../../components/Navbar";
import SearchForm from "../../components/SearchForm";
import ResultsList from "../../components/ResultsList";

const Search = () => {
  const [input, setInput] = useState("");
  const [option, setOption] = useState("");
  const inputDataHandler = (inputValue) => {
    setInput(inputValue);
  };
  const optionChangeHander = (optionValue) => {
    setOption(optionValue);
  };
  return (
    <>
      <div id="movie-detail-root"></div>
      <div className="app">
        <Navbar />
        <SearchForm inputData={inputDataHandler} option={optionChangeHander} />
        <ResultsList inputData={input} option={option} />
      </div>
    </>
  );
};

export default Search;
