import { useEffect, useState } from "react";
import DetailMovie from "./DetailMovie";
import axios from "axios";

import styles from "./ResultsList.module.css";

const ResultsList = (props) => {
  const [searchList, setSearchList] = useState(null);
  const typeSearch = props.inputData.typeSearch;
  const inputData = props.inputData.input;
  const [option, setOption] = useState(props.option);

  useEffect(() => {
    axios
      .post("http://localhost:5000/api/movies/search?token=8qlOkxz4wq", {
        keyword: inputData,
        typeSearch: typeSearch,
        option: undefined,
      })
      // fetch(
      //   `http://localhost:5000/api/movies/search?token=8qlOkxz4wq&keyword=${inputData}&typeSearch=${typeSearch}&option=${undefined}`
      // )
      .then((response) => response.json())
      .then((data) => setSearchList(data.results));
  }, [inputData, typeSearch]);

  useEffect(() => {
    if (option === "en-us" || option === "kr" || option === "jp") {
      axios
        .post("http://localhost:5000/api/movies/search?token=8qlOkxz4wq", {
          keyword: "a",
          typeSearch: "Language",
          option: option,
        })
        // fetch(
        //   `http://localhost:5000/api/movies/search?token=8qlOkxz4wq&keyword=${"a"}&typeSearch=${"Language"}&option=${option}`
        // )
        .then((response) => response.json())
        .then((data) => setSearchList(data.results));
    } else {
      if (
        option === "all" ||
        option === "tv" ||
        option === "movie" ||
        option === "person"
      ) {
        // fetch(
        //   `http://localhost:5000/api/movies/search?token=8qlOkxz4wq&keyword=${"a"}&typeSearch=${"MediaType"}&option=${option}`
        // )
        axios
          .post("http://localhost:5000/api/movies/search?token=8qlOkxz4wq", {
            keyword: "a",
            typeSearch: "MediaType",
            option: option,
          })
          .then((response) => response.json())
          .then((data) => setSearchList(data.results));
      } else {
        // fetch(
        //   `http://localhost:5000/api/movies/search?token=8qlOkxz4wq&keyword=${"a"}&typeSearch=${"Year"}&option=${option}`
        // )
        axios
          .post("http://localhost:5000/api/movies/search?token=8qlOkxz4wq", {
            keyword: "a",
            typeSearch: "Year",
            option: option,
          })
          .then((response) => response.json())
          .then((data) => setSearchList(data.results));
      }
    }
  }, [option]);

  useEffect(() => {
    setOption(props.option);
  }, [props.option]);

  //   Render từng phần tử
  const resultRender = [];
  if (searchList) {
    for (let i = 0; i < searchList.length; i += 8) {
      resultRender.push(searchList.slice(i, i + 8));
    }
  }

  return (
    <div className={styles["results-list"]}>
      <h2>Search Results</h2>
      <div className={styles["results-container"]}>
        <div className={styles["row-result"]}>
          <RowResult dataRender={resultRender[0]} />
        </div>
        <div className={styles["row-result"]}>
          <RowResult dataRender={resultRender[1]} />
        </div>
        <div className={styles["row-result"]}>
          <RowResult dataRender={resultRender[2]} />
        </div>
      </div>
    </div>
  );
};

const RowResult = (props) => {
  const data = props.dataRender;
  const [hoveredItem, setHoveredItem] = useState(null);
  const [selectedMovie, setSelectedMovie] = useState(null);

  const handlerMouseEnter = (ItemID) => {
    setHoveredItem(ItemID);
  };
  const handerMouseLeave = () => {
    setHoveredItem(null);
  };
  const handlerModalMovieDetail = (dataMovie) => {
    if (selectedMovie === null || dataMovie.id !== selectedMovie.id) {
      setSelectedMovie(dataMovie);
    } else {
      setSelectedMovie(null);
    }
  };

  // console.log(data);
  return (
    <>
      {data &&
        data.map((el) => {
          return (
            <div key={el.id} className={styles["result-element"]}>
              <div
                key={el.id}
                className={`${hoveredItem === el.id ? styles.active : ""}`}
                onMouseEnter={() => handlerMouseEnter(el.id)}
                onMouseLeave={handerMouseLeave}
                onClick={() => handlerModalMovieDetail(el)}
              >
                <img
                  src={`https://image.tmdb.org/t/p/w500${el.poster_path}`}
                  alt=""
                />
              </div>
            </div>
          );
        })}
      <div className={styles["movie-detail"]}>
        {selectedMovie && <DetailMovie dataModal={selectedMovie} />}
      </div>
    </>
  );
};

export default ResultsList;
