import React, { useRef, useState } from "react";
import { LeftOutlined, RightOutlined } from "@ant-design/icons";

import styles from "./MoviesListComponent.module.css";
import DetailMovie from "./DetailMovie";

const MoviesListComponent = React.memo((props) => {
  const contentRef = useRef(null);
  const [scrollPosition, setScrollPosition] = useState(0);
  const [hoveredItem, setHoveredItem] = useState(null);
  const [selectedMovie, setSelectedMovie] = useState(null);

  const handlerMouseEnter = (ItemID) => {
    setHoveredItem(ItemID);
  };
  const handerMouseLeave = () => {
    setHoveredItem(null);
  };
  //  xử lý scroll phim
  const handlerScrollLeft = () => {
    contentRef.current.scrollLeft = scrollPosition - 500;
    setScrollPosition(!(scrollPosition <= 0) ? scrollPosition - 500 : 3500);
  };
  const handlerScrollRight = () => {
    contentRef.current.scrollLeft = scrollPosition + 500;
    setScrollPosition(scrollPosition < 4000 ? scrollPosition + 500 : -500);
  };
  // Xử lý click tạo movie detail
  const handlerModalMovieDetail = (dataMovie) => {
    if (selectedMovie === null || dataMovie.id !== selectedMovie.id) {
      setSelectedMovie(dataMovie);
    } else {
      setSelectedMovie(null);
    }
  };

  // Xử lý render filmList
  const dataRender = props.data.map((el) => {
    return (
      <>
        <div
          key={el.id}
          className={`${
            props.valid ? styles["original-section"] : styles["orther-section"]
          } 
        ${styles["movie-element"]}
        `}
          onMouseEnter={() => handlerMouseEnter(el.id)}
          onMouseLeave={handerMouseLeave}
          onClick={() => handlerModalMovieDetail(el)}
        >
          <div className={`${hoveredItem === el.id ? styles.active : ""}`}>
            <img
              src={
                props.valid
                  ? `https://image.tmdb.org/t/p/w500${el.poster_path}`
                  : `https://image.tmdb.org/t/p/w500${el.backdrop_path}`
              }
              alt=""
            />
          </div>
        </div>
      </>
    );
  });
  // return
  return (
    <>
      <div className={styles["movieslist-component"]}>
        <button className={styles["btn-left"]} onClick={handlerScrollLeft}>
          <LeftOutlined style={{ color: "#fff", fontSize: "36px" }} />
        </button>
        <div
          className={`${styles["container-movies"]} ${
            props.valid
              ? styles["original-sections"]
              : styles["orther-sections"]
          }`}
          ref={contentRef}
        >
          {dataRender}
        </div>
        <button className={styles["btn-right"]} onClick={handlerScrollRight}>
          <RightOutlined style={{ color: "#fff", fontSize: "36px" }} />
        </button>
      </div>
      {selectedMovie && <DetailMovie dataModal={selectedMovie} />}
      {/* {movieDetail} */}
    </>
  );
});

export default MoviesListComponent;
