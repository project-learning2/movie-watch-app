import React, { useState, useEffect } from "react";

import styles from "./DetailMovie.module.css";

const DetailMovie = (props) => {
  const movieData = props.dataModal;
  const [urlTrailer, setUrlTrailer] = useState("");

  useEffect(() => {
    fetch(
      `http://localhost:5000/api/movies/video?token=8qlOkxz4wq&id=${movieData.id}`
    )
      .then((response) => response.json())
      .then((data) =>
        setUrlTrailer(`https://www.youtube.com/embed/${data.key}`)
      );
  }, [setUrlTrailer, movieData]);

  return (
    <div className={styles["movie_detail"]}>
      <div className={styles["movie_detail_data"]}>
        <h2>{movieData.title ? movieData.title : movieData.name}</h2>
        <h4>Release Date: {movieData.release_date}</h4>
        <h4>Vote: {movieData.vote_average}</h4>
        <p>{movieData.overview}</p>
      </div>
      <div>
        <div className={styles["movie_detail_trailer"]}>
          <iframe
            title={movieData.id}
            width="100%"
            height="600px"
            src={urlTrailer}
          ></iframe>
        </div>
      </div>
    </div>
  );
};

export default DetailMovie;
