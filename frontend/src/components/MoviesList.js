import { useState, useEffect, useCallback } from "react";

import styles from "./MoviesList.module.css";
import MoviesListComponent from "./MoviesListComponent";

const MoviesList = () => {
  function useFetchData(url) {
    const [dataMovies, setDataMovies] = useState([]);
    const fetchMoviesAPI = useCallback(async () => {
      try {
        const response = await fetch(url);
        if (!response.ok) {
          throw new Error("Something went wrong!");
        }
        const data = await response.json();
        setDataMovies(data.results);
      } catch (error) {
        console.log(error);
      }
    }, [url]);
    useEffect(() => {
      fetchMoviesAPI();
    }, [fetchMoviesAPI]);
    // console.log(dataMovies);
    return { dataMovies };
  }

  const originalList = useFetchData(
    "http://localhost:5000/api/movies?token=8qlOkxz4wq&genreId=12"
  ).dataMovies;
  const trendingList = useFetchData(
    "http://localhost:5000/api/movies/trending?token=8qlOkxz4wq"
  ).dataMovies;
  const topRatedList = useFetchData(
    "http://localhost:5000/api/movies/top-rate?token=8qlOkxz4wq"
  ).dataMovies;
  const actionList = useFetchData(
    "http://localhost:5000/api/movies?token=8qlOkxz4wq&genreId=28"
  ).dataMovies;
  const comedyList = useFetchData(
    "http://localhost:5000/api/movies?token=8qlOkxz4wq&genreId=35"
  ).dataMovies;
  const horrorList = useFetchData(
    "http://localhost:5000/api/movies?token=8qlOkxz4wq&genreId=27"
  ).dataMovies;
  const romanceList = useFetchData(
    "http://localhost:5000/api/movies?token=8qlOkxz4wq&genreId=10749"
  ).dataMovies;
  const documentList = useFetchData(
    "http://localhost:5000/api/movies?token=8qlOkxz4wq&genreId=99"
  ).dataMovies;

  const IsOriginalList = true;
  return (
    <div className={styles["movies-list"]}>
      <h3>Adventure</h3>
      <MoviesListComponent data={originalList} valid={IsOriginalList} />
      <h3>Trending List</h3>
      <MoviesListComponent data={trendingList} valid={!IsOriginalList} />
      <h3>Top Rated</h3>
      <MoviesListComponent data={topRatedList} valid={!IsOriginalList} />
      <h3>Actions</h3>
      <MoviesListComponent data={actionList} valid={!IsOriginalList} />
      <h3>Comedy</h3>
      <MoviesListComponent data={comedyList} valid={!IsOriginalList} />
      <h3>Horror</h3>
      <MoviesListComponent data={horrorList} valid={!IsOriginalList} />
      <h3>Romance</h3>
      <MoviesListComponent data={romanceList} valid={!IsOriginalList} />
      <h3>Document</h3>
      <MoviesListComponent data={documentList} valid={!IsOriginalList} />
    </div>
  );
};

export default MoviesList;
