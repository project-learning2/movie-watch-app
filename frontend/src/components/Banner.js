import { useEffect, useState } from "react";

import styles from "./Banner.module.css";

const Banner = () => {
  const [dataMovies, setDataMovies] = useState([]);

  useEffect(() => {
    fetch("http://localhost:5000/api/movies/trending?token=8qlOkxz4wq")
      .then((response) => response.json())
      .then((data) => setDataMovies(data.results));
  }, [setDataMovies]);

  const dataRender =
    dataMovies[Math.floor(Math.random() * dataMovies.length - 1)];

  function truncate(str, n) {
    return str?.length > n ? str.substr(0, n - 1) + "..." : str;
  }

  const content = dataRender ? (
    <header
      className={styles.banner}
      style={{
        backgroundSize: "cover",
        backgroundImage: `url(
      "https://image.tmdb.org/t/p/original/${dataRender.backdrop_path}"
      )`,
        backgroundPosition: "center center",
      }}
    >
      <div className={styles["banner_contents"]}>
        <h1 className={styles["banner_title"]}>
          {dataRender.title ? dataRender.title : dataRender.name}
        </h1>
        <div className={styles["banner_buttons"]}>
          <button className={styles["banner_button"]}>Play</button>
          <button className={styles["banner_button"]}>My List</button>
        </div>
        <h1 className={styles["banner_description"]}>
          {truncate(dataRender.overview, 150)}
        </h1>
      </div>
    </header>
  ) : (
    <p>Loadingggggg........</p>
  );

  return <>{content}</>;
};

export default Banner;
