const path = require("path");

const express = require("express");

const movieController = require("../controllers/movie");
const genreController = require("../controllers/genre");
const videoController = require("../controllers/video");
const searchController = require("../controllers/search");

const router = express.Router();

// Get TOPTRENDIGN
//  /api/movies/trending => Get
router.get("/api/movies/trending", movieController.getTopTrendingFirst);
//  /api/movies/trending/page => Get
router.get("/api/movies/trending/:page", movieController.getTopTrending);

// Get TopRate
// /api/movies/top-rate => get
router.get("/api/movies/top-rate", movieController.getTopRateFirst);
// /api/movies/top-rate/:page => get
router.get("/api/movies/top-rate/:page", movieController.getTopRate);

// Get  Lấy các phim theo thể loại
//  /api/movies => get
router.get("/api/movies", genreController.getGenreList);

// Lấy trailer của một bộ phim
///api/movies/video => get
router.post("/api/movies/video", videoController.postTrailerVideo);

// Tìm kiếm phim theo từ khóa
//
router.post("/api/movies/search", searchController.postResultsSearch);

module.exports = router;
