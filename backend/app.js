const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");

const cors = require("cors");
const app = express();
const tokenController = require("./controllers/token");
const errorController = require("./controllers/error");

// const users = [];
const movieRoutes = require("./routes/movie");

app.use(cors());
// app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", tokenController.tokenverifi);
app.use("/", movieRoutes);

app.use(errorController.get404);

app.listen(5000);
