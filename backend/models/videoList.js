const fs = require("fs");
const path = require("path");

const videoListPath = path.join(
  path.dirname(process.mainModule.filename),
  "data",
  "videoList.json"
);

const getVideoFromFile = (cb) => {
  fs.readFile(videoListPath, (err, fileContent) => {
    if (err) {
      cb([]);
    } else {
      cb(JSON.parse(fileContent));
    }
  });
};

module.exports = class VideoList {
  constructor() {}

  static fetchAll(cb) {
    getVideoFromFile(cb);
  }
};
