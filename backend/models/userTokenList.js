const fs = require("fs");
const path = require("path");

const userTokenPath = path.join(
  path.dirname(process.mainModule.filename),
  "data",
  "userToken.json"
);

const getUserTokenFromFile = (cb) => {
  fs.readFile(userTokenPath, (err, fileContent) => {
    if (err) {
      cb([]);
    } else {
      cb(JSON.parse(fileContent));
    }
  });
};

module.exports = class UserTokenList {
  constructor() {}
  static fetchAll(cb) {
    getUserTokenFromFile(cb);
  }
};
