const fs = require("fs");
const path = require("path");

const genreListPath = path.join(
  path.dirname(process.mainModule.filename),
  "data",
  "genreList.json"
);

const getGenreFromFile = (cb) => {
  fs.readFile(genreListPath, (err, fileContent) => {
    if (err) {
      cb([]);
    } else {
      cb(JSON.parse(fileContent));
    }
  });
};

module.exports = class GenreList {
  constructor() {}

  static fetchAll(cb) {
    getGenreFromFile(cb);
  }
};
