const fs = require("fs");
const path = require("path");

const moviesListPath = path.join(
  path.dirname(process.mainModule.filename),
  "data",
  "movieList.json"
);

const getMovieListFromFile = (cb) => {
  fs.readFile(moviesListPath, (err, fileContent) => {
    if (err) {
      cb([]);
    } else {
      cb(JSON.parse(fileContent));
    }
  });
};

module.exports = class MovieList {
  constructor() {}

  static fetchAll(cb) {
    getMovieListFromFile(cb);
  }
};
