const MovieList = require("../models/movieList");

//  /api/movies/trending/page => Get
exports.getTopTrending = (req, res, next) => {
  MovieList.fetchAll((movieList) => {
    movieList.sort((a, b) => b.popularity - a.popularity);
    const totalMovies = movieList.length;
    const totalPages = Math.trunc(totalMovies / 20) + 1;
    const resultPerPage = [];
    const page = req.params.page;

    for (let i = 0; i < movieList.length; i += 20) {
      resultPerPage.push(movieList.slice(i, i + 20));
    }

    const responseData = {
      results: resultPerPage[page - 1],
      page: page,
      total_pages: totalPages,
    };
    res.status(200).json(responseData);
  });
};

//  /api/movies/trending => Get
exports.getTopTrendingFirst = (req, res, next) => {
  MovieList.fetchAll((movieList) => {
    movieList.sort((a, b) => b.popularity - a.popularity);
    const totalMovies = movieList.length;
    const totalPages = Math.trunc(totalMovies / 20) + 1;
    const resultPerPage = [];

    for (let i = 0; i < movieList.length; i += 20) {
      resultPerPage.push(movieList.slice(i, i + 20));
    }
    const responseData = {
      results: resultPerPage[0],
      page: 1,
      total_pages: totalPages,
    };
    res.status(200).json(responseData);
  });
};
// /api/movies/top-rate => get
exports.getTopRateFirst = (req, res, next) => {
  MovieList.fetchAll((movieList) => {
    movieList.sort((a, b) => b.vote_average - a.vote_average);
    const totalMovies = movieList.length;
    const totalPages = Math.trunc(totalMovies / 20) + 1;
    const resultPerPage = [];

    for (let i = 0; i < movieList.length; i += 20) {
      resultPerPage.push(movieList.slice(i, i + 20));
    }
    const responseData = {
      results: resultPerPage[0],
      page: 1,
      total_pages: totalPages,
    };
    res.status(200).json(responseData);
  });
};
//// /api/movies/top-rate/:page => get
exports.getTopRate = (req, res, next) => {
  MovieList.fetchAll((movieList) => {
    movieList.sort((a, b) => b.vote_average - a.vote_average);
    const totalMovies = movieList.length;
    const totalPages = Math.trunc(totalMovies / 20) + 1;
    const resultPerPage = [];
    const page = req.params.page;

    for (let i = 0; i < movieList.length; i += 20) {
      resultPerPage.push(movieList.slice(i, i + 20));
    }

    const responseData = {
      results: resultPerPage[page - 1],
      page: page,
      total_pages: totalPages,
    };
    res.status(200).json(responseData);
  });
};
