const VideoList = require("../models/videoList");

///api/movies/video => get
exports.postTrailerVideo = (req, res, next) => {
  const videoId = req.query.id;
  if (!videoId) {
    const message = "Not found film_id parram";
    res.json({ message });
  } else {
    VideoList.fetchAll((videoList) => {
      const trailerVideo = [];
      videoList.forEach((el) => {
        if (el.id == videoId) {
          el.videos.forEach((video) => {
            if (
              video.official &&
              video.site === "YouTube" &&
              (video.type === "Trailer" || video.type === "Teaser")
            ) {
              if (video.type === "Trailer") {
                trailerVideo.push(video);
              } else {
                trailerVideo.push(video);
              }
            }
          });
        }
      });

      const videoresponse = trailerVideo.sort(
        (a, b) => new Date(b.published_at) - new Date(a.published_at)
      )[0];
      if (videoresponse) {
        res.json(videoresponse);
      } else {
        const message = "Not found video";
        res.status(404).json({ message });
      }
    });
  }
};
