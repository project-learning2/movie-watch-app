const GenreList = require("../models/genreList");
const MovieList = require("../models/movieList");

// /api/movies => get
exports.getGenreList = (req, res, next) => {
  GenreList.fetchAll((genreList) => {
    const genreId = req.query.genreId;
    const page = req.query.page;
    const genre = genreList.find((item) => item.id == genreId);
    if (!genreId) {
      const message = "Not found gerne parram";
      res.status(400).json({ message });
      return;
    } else {
      if (!genre) {
        const message = "Not found that genre id";
        res.status(400).json({ message });
        return;
      } else {
        MovieList.fetchAll((movieList) => {
          const moviesGenreList = [];
          movieList.forEach((elMovie) => {
            elMovie.genre_ids.forEach((idGenre) => {
              if (idGenre == genre.id) {
                moviesGenreList.push(elMovie);
              }
            });
          });

          const totalResults = moviesGenreList.length;
          const totalPages = Math.trunc(totalResults / 20) + 1;
          const resultPage = [];

          for (let i = 0; i < totalResults; i += 20) {
            resultPage.push(moviesGenreList.slice(i, i + 20));
          }
          if (!page) {
            const responseData = {
              results: resultPage[0],
              page: 1,
              total_pages: totalPages,
            };
            res.status(200).json(responseData);
          } else {
            const responseData = {
              results: resultPage[page - 1],
              page: page,
              total_pages: totalPages,
            };
            res.status(200).json(responseData);
          }
        });
      }
    }
  });
};
