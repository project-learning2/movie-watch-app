const MoviesList = require("../models/movieList");
const GenreList = require("../models/genreList");

exports.postResultsSearch = (req, res, next) => {
  // Logic search
  const keyword = req.body.keyword.toLowerCase();
  const typeSearch = req.body.typeSearch;
  const option = req.body.option;
  console.log("hello", keyword, typeSearch, option);

  MoviesList.fetchAll((moviesList) => {
    GenreList.fetchAll((genreList) => {
      const resultsList = [];
      if (
        !keyword &&
        (typeSearch === "NameContent" || typeSearch === "Genre")
      ) {
        const message = "Not found keyword parram";
        res.status(400).json({ message });
      } else {
        //  Có thể dùng switch case để xử lý logic này
        if (typeSearch === "NameContent" || !typeSearch) {
          moviesList.forEach((el) => {
            if (el.title) {
              const title = el.title.toLowerCase();
              if (title.includes(keyword)) {
                resultsList.push(el);
              } else {
                if (el.overview) {
                  const overview = el.overview.toLowerCase();
                  if (overview.includes(keyword)) {
                    resultsList.push(el);
                  }
                } else {
                  if (el.name) {
                    const name = el.name.toLowerCase();
                    if (name.includes(keyword)) {
                      resultsList.push(el);
                    }
                  }
                }
              }
            }
          });
        } else {
        }
        // xử lý search với genre
        if (typeSearch === "Genre") {
          const genreListResults = [];
          genreList.forEach((el) => {
            const nameGenre = el.name.toLowerCase();
            if (nameGenre.includes(keyword)) {
              genreListResults.push(el);
            }
          });

          const genreIdResultsList = genreListResults.map((el) => {
            return el.id;
          });

          genreIdResultsList.forEach((idGenre) => {
            moviesList.forEach((movie) => {
              movie.genre_ids.forEach((el) => {
                if (el == idGenre) {
                  resultsList.push(movie);
                  // Lọc lại resultList vì có nhiều phần tử trùng
                }
              });
            });
          });
        }

        // Xử lý với mediaType
        if (typeSearch === "MediaType") {
          moviesList.forEach((movie) => {
            if (option === "all") {
              resultsList.push(movie);
            } else {
              if (movie.media_type === option) {
                resultsList.push(movie);
              }
            }
          });
        }
        // Xử lý theo language
        if (typeSearch === "Language") {
          if (option === "en-us") {
            moviesList.forEach((movie) => {
              if (movie.original_language === "en") {
                resultsList.push(movie);
              }
            });
          }
          if (option === "kr") {
            moviesList.forEach((movie) => {
              if (movie.original_language === "ko") {
                resultsList.push(movie);
              }
            });
          }
          if (option === "jp") {
            moviesList.forEach((movie) => {
              if (movie.original_language === "ja") {
                resultsList.push(movie);
              }
            });
          }
        }
        if (typeSearch === "Year") {
          moviesList.forEach((movie) => {
            if (movie.release_date) {
              movie.release_date.slice(0, 4) === option &&
                resultsList.push(movie);
            }
            if (movie.first_air_date) {
              movie.first_air_date.slice(0, 4) === option &&
                resultsList.push(movie);
            }
          });
          // console.log(movieResult);
        }

        //   Paging
        const totalResults = resultsList.length;
        const totalPages = Math.trunc(totalResults / 20) + 1;
        const resultPerPage = [];
        let page;

        for (let i = 0; i < totalResults; i += 20) {
          resultPerPage.push(resultsList.slice(i, i + 20));
        }

        if (!page) {
          const responseData = {
            results: resultPerPage[0],
            page: 1,
            total_pages: totalPages,
          };
          res.status(200).json(responseData);
        } else {
          const responseData = {
            results: resultPerPage[page - 1],
            page: page,
            total_pages: totalPages,
          };
          // console.log(responseData);
          res.status(200).json(responseData);
        }
      }
    });
  });
};
