exports.get404 = (req, res, next) => {
  const message = "Route not found";
  res.status(404).json({ message });
};
