const UserTokenList = require("../models/userTokenList");

exports.tokenverifi = (req, res, next) => {
  const token = req.query.token;
  UserTokenList.fetchAll((userToken) => {
    const user = userToken.find((user) => user.token === token);
    if (user) {
      next();
    } else {
      const message = "Unauthorized";
      res.status(401).json({ message });
    }
  });
};
